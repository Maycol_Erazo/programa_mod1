/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio_modulo1;

import java.util.Arrays;

/**
 *
 * @author mike
 */
public class Arreglo_con_ciudades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         String[] ciudades={
          "1. Nueva York",
          "2. Amsterdam",
          "3. Shanghai",
          "4. Moscú",
          "5. Toronto",
          "6. Melbourne",
          "7. Madrid",
          "8. Berlín",
          "9. Seúl",
          "10. Bruselas",
          "11. Sydney",
          "12. Washington",
          "13. Beijing",
          "14. Chicago",
          "15. Los Angeles",
          "16. Singapur",
          "17. Hong Kong",
          "18. Tokio",
          "19. París",
          "20. Londres"
        
      
        };
  
        Arrays.stream(ciudades).forEach(System.out::println);
        
    }
    
}
